class CreateResourceTrades < ActiveRecord::Migration[5.1]
  def change
    create_table :resource_trades do |t|
      t.integer :water
      t.integer :food
      t.integer :medication
      t.integer :ammunition
      t.references :survivor, foreign_key: true

      t.timestamps
    end
  end
end
