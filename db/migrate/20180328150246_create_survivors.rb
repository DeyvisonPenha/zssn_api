class CreateSurvivors < ActiveRecord::Migration[5.1]
  def change
    create_table :survivors do |t|
      t.string :name
      t.integer :age
      t.string :gender
      t.float :lat
      t.float :long
      t.integer :report,  null:false, default: 0

      t.timestamps
    end
  end
end
