class CreateResources < ActiveRecord::Migration[5.1]
  def change
    create_table :resources do |t|
      t.integer :water
      t.integer :food
      t.integer :medication
      t.integer :ammunition
      t.references :survivor, foreign_key: true

      t.timestamps
    end
  end
end
