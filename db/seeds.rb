# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Survivor.create(name:"fulano",age:27,gender:"male",lat:55.8,long:47.9)
Survivor.create(name:"ciclano",age:21,gender:"female",lat:45.8,long:87.9)
Survivor.create(name:"joao",age:20,gender:"female",lat:95.8,long:17.9)
Survivor.create(name:"teste",age:21,gender:"female",lat:45.8,long:87.9,report:1)
Survivor.create(name:"maria",age:20,gender:"female",lat:95.8,long:17.9,report:3)

Survivor.find_by(name:"fulano").create_resource(water:1,food:5,medication:8,ammunition:4)
Survivor.find_by(name:"ciclano").create_resource(water:3,food:2,medication:1,ammunition:10)
Survivor.find_by(name:"joao").create_resource(water:4,food:5,medication:8,ammunition:4)
Survivor.find_by(name:"teste").create_resource(water:4,food:9,medication:0,ammunition:6)
Survivor.find_by(name:"maria").create_resource(water:4,food:9,medication:0,ammunition:6)


Survivor.find_by(name:"fulano").create_resource_trade(water:1,food:2,medication:1)
Survivor.find_by(name:"ciclano").create_resource_trade(water:1,ammunition:1)
Survivor.find_by(name:"joao").create_resource_trade(water:2,food:1,ammunition:1)
Survivor.find_by(name:"teste").create_resource_trade(water:2,food:1,ammunition:1)
Survivor.find_by(name:"maria").create_resource_trade(ammunition:6)

