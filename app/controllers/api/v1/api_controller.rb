module Api::V1
 
 class ApiController < ApplicationController
 
   # Métodos globais
   private
    # Use callbacks to share common setup or constraints between actions.
    def set_survivor_resource
    	if Survivor.find(params[:survivor_id]).report ==3
    		@resource ||= nil
    	else
    		@current_surv = Survivor.find(params[:survivor_id])
    	end
    end
 
 end
 
end