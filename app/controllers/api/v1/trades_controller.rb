class Api::V1::TradesController < Api::V1::ApiController

	def index
		@trades = Trade.all

		render json: @trades
	end

	def create
		@trades = Trade.new(trades_params)

		if @trades.save
			render json: @trades
		else
			render json: @trades.errors, status: :unprocessable_entity
		end
		
	end

	private
    
    # Only allow a trusted parameter "white list" through.
    def trades_params #resources params are return empty
      params.require(:trade).permit(:survivor1_id, :survivor2_id)
    end

end
