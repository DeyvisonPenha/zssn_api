class Api::V1::SurvivorsController < Api::V1::ApiController

	before_action :set_survivor, only: [:show, :update, :destroy]

  # GET /survivors
  def index
    @survivors = Survivor.where.not(report: 3).all

    render json: @survivors
  end

  # GET /survivors/1
  def show
    render json: @survivor
  end

  # POST /survivors
  def create
    # Create survivor and yours resources at same time
    @survivor = Survivor.new(survivor_params)
    @survivor.build_resource(resource_params)
    #create a empty repository to update in trade operation
    @survivor.build_resource_trade()
    # if save show both
    if @survivor.save    
      response = { survivor: @survivor, resources: @resources }
      render json: response
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /survivors/1
  def update
    if @survivor.update(survivor_params)
      render json: @survivor
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end

  # DELETE /survivors/1
  #def destroy
  #  @survivor.destroy
  #end

  def reportInf
    survivor_to_update
    if @survivor.update(report: @survivor.report+1)
      render json: @survivor
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end

  def lastposition
    survivor_to_update
    if @survivor.update(lat: params[:lat],long: params[:long])
      render json: @survivor
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_survivor
      @survivor = Survivor.find(params[:id])
      if @survivor.report == 3 and !@survivor.report.nil?
        @survivor = nil
      end
    end

    def survivor_to_update
      @survivor = Survivor.find(params[:survivor_id])
    end

    # Only allow a trusted parameter "white list" through.
    def survivor_params #resources params are return empty
      params.require(:survivor).permit(:name, :age, :gender, :lat, :long, :report, resource: [:water, :food, :medication, :ammunition])
    end

    def resource_params
      @resources = {water:params[:water],food:params[:food],
      medication:params[:food],ammunition:params[:food]}
    end

end
