class Api::V1::ResourcesTradeController < Api::V1::ApiController
	
	before_action :set_survivor_resource , only: [:index, :create]

	def index
		@aux_res_param = @current_surv.resource_trade
		puts "aux_res_param #{@aux_res_param}"
		render json:@current_surv.resource_trade
	end

	def create
		@current_surv.resource_trade.update(clear_resource_params)

		if @current_surv.resource_trade.update(resource_trade_params)
			@resource_trade ||= @current_surv.resource_trade
			render json: @resource_trade
		else
			render json: @resource_trade.errors, status: :unprocessable_entity
		end
	end

	private

	def resource_trade_params
		params.require(:resources_trade).permit(:water, :food, :medication, :ammunition, :survivor_id)
	end

	def clear_resource_params
      @resources = {water:0,food:0,
      medication:0,ammunition:0}
    end
end
