class Api::V1::ResourcesController < Api::V1::ApiController

	before_action :set_survivor_resource, only: [:index, :update, :create]

  # GET /resources
  def index
    @resource ||= @current_surv.resource

    render json: @resource
  end


  private
    
    # Only allow a trusted parameter "white list" through.
    def resource_params
      params.require(:resource).permit(:water, :food, :medication, :ammunition, :survivor_id)
    end

end
