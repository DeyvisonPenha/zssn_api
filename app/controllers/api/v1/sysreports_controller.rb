class Api::V1::SysreportsController < Api::V1::ApiController

	def index
		perc_infec
		perc_not_infect
		meanresources
		avoidpoints
		
		render json: {perc_infec: @survivors_infected ,
			perc_not_infect: @survivors_not_infected,
			point_loss: @point_loss,
			meanresources: {water:@menwater,food:@menwfood,
			medication:@menmedication,ammunition:@menammunition} }
	end



	def perc_infec
		@survivors_infected = ((Survivor.where("report >= ?",3).count*100).to_f/(Survivor.all.count).to_f).round(2)
	end

	def perc_not_infect
		@survivors_not_infected = ((Survivor.where("report < ?",3).count*100).to_f/(Survivor.all.count).to_f).round(2)
	end

	def meanresources
		surv = Survivor.where("report < ?",3)
		
		count_water=0; 
		count_food=0; 
		count_medication=0; 
		count_ammunition=0; 

		surv.each do |k| 
			count_water = count_water + k.resource.water 
			count_food = count_food + k.resource.food 
			count_medication = count_medication + k.resource.medication
			count_ammunition = count_ammunition + k.resource.ammunition 
		end
		
		@menwater = count_water.to_f/surv.count.to_f
		@menwfood = count_food.to_f/surv.count.to_f
		@menmedication = count_medication.to_f/ surv.count.to_f
		@menammunition = count_ammunition.to_f/ surv.count.to_f
	end

	def avoidpoints

		surv = Survivor.where("report = ?",3).all
		@point_loss = 0; 
		
		surv.each do |k| 
			@point_loss = @point_loss + Trade.points(k.id) 
		end
		
	end
	
end
