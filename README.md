# ZSSN (Zombie Survival Social Network)

> This API was created in order to help non-infected survivors share their resources in an apocalyptic scenario.

Project created as part of an [admission test](https://bitbucket.org/outbound-marketing/backend-challenge/) at [Reev ](http://www.reev.co/).

## Requirements

This API was make using ```sh ruby 2.4.1``` and ```sh rails 5```, so before installation, make sure you have both installed on your machine.

## Dependencies

Once you have cloned this project:

```sh
$ git clone https://DeyvisonPenha@bitbucket.org/DeyvisonPenha/zombi_survival.git
```

Run the following commands to install all dependencies:

```sh
$ cd zssn/
$ bundle install
```

## Database
Sqlite3 was used as the database for the application. The following commands will prepare the database:

```sh
$ rake db:drop db:create db:migrate db:seed
```

## Running

```sh
$ rails s
```

And you'll have access at [localhost:3000](http://localhost:3000/). you can navigate through the routes in the browser , but I suggest that you use [HTTPie](https://httpie.org/) to do these operations like in the examples.

## API Documentation

Here you'll find all API endpoints and how to use them.

### GET /survivors
Gets all survivors and their resources.

#### Request
```sh
http://localhost:3000/survivors
```
or by HTTPie:
```sh
http :3000/survivors
```

#### Response

```sh
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"c128b69a0dfc2973f4feac080e61f4da"
Transfer-Encoding: chunked
X-Request-Id: 211e14c6-0304-430f-92f5-3d3eed04c71f
X-Runtime: 0.033090

[
    {
        "age": 27, 
        "created_at": "2018-03-27T02:03:52.162Z", 
        "gender": "male", 
        "id": 1, 
        "lat": 55.8, 
        "long": 47.9, 
        "name": "fulano", 
        "report": 0, 
        "updated_at": "2018-03-27T02:03:52.162Z"
    }, 
    {
        "age": 21, 
        "created_at": "2018-03-27T02:03:52.240Z", 
        "gender": "female", 
        "id": 2, 
        "lat": 45.8, 
        "long": 87.9, 
        "name": "ciclano", 
        "report": 0, 
        "updated_at": "2018-03-27T02:03:52.240Z"
    }, 
    ...
]
```

### POST /survivors
Creates a survivor as well as declares all of his resources. For the problem system is needed to create both survivors and resources at the same time.

#### Request
```sh
http POST :3000/survivors name=Fox age=52 water=1 food=6
```

##### Survivors Parameters

Parameter      | Type    | Description
-------------- | ------- | -----------
:name          | String  | Survivor's name
:age           | Integer | Survivor's age
:gender        | String  | Survivor's gender (e.g. 'Male')
:report        | Integer | Survivor's flag to infected
:lat           | Float   | Survivor's latitude location
:long          | Float   | Survivor's longitude location

##### Resources Parameters

Parameter      | Type    | Description
-------------- | ------- | -----------
:water         | Integer | Survivor's resource of water
:food          | Integer | Survivor's resource of food
:medication    | Integer | Survivor's resource of medication
:ammunition    | Integer | Survivor's resource of ammunition
:ammunition    | Integer | Survivor's resource of ammunition
:survivor_id   | Integer | Survivor's references



#### Response
```sh
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"6460ccd7d324a30587c5b2a1069d8497"
Transfer-Encoding: chunked
X-Request-Id: 7763debd-b04c-4e1f-b167-e4a3c4c82be1
X-Runtime: 0.107297

{
    "resources": {
        "ammunition": "6", 
        "food": "6", 
        "medication": "6", 
        "water": "1"
    }, 
    "survivor": {
        "age": 52, 
        "created_at": "2018-03-27T02:16:29.950Z", 
        "gender": null, 
        "id": 6, 
        "lat": null, 
        "long": null, 
        "name": "fasdfaasdf", 
        "report": 0, 
        "updated_at": "2018-03-27T02:16:29.950Z"
    }
}

```

### Update lastposition
### /survivors/:id/updatelocation
Updates survivor's current location.

#### Request
```sh
http POST :3000/survivors/1/updatelocation lat=52.68 long=-52.7
```
##### Parameters

Parameter      | Type    | Description
-------------- | ------- | -----------
lat 		   | float   | Last location updated containing latitude
long           | float   | Last location updated containing longitude

#### Response
```sh
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"2384cc8b83e103e1e0f5c6ac502753dd"
Transfer-Encoding: chunked
X-Request-Id: c04dd2ae-113b-47a9-bdb4-105f8a97ed44
X-Runtime: 0.069950

{
    "age": 27, 
    "created_at": "2018-03-27T02:03:52.162Z", 
    "gender": "male", 
    "id": 1, 
    "lat": 52.68, 
    "long": -52.7, 
    "name": "fulano", 
    "report": 0, 
    "updated_at": "2018-03-27T02:20:52.645Z"
}
```


### POST /survivors/:id/flaginfects
Flags a survivor as infected. A survivor who has 3 flags will be considered infected.


#### Request
```sh
http :3000/survivors/2/flaginfects
```

#### Response
```sh
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"cbfc1456c0dffd1d79b4d8b870460b0b"
Transfer-Encoding: chunked
X-Request-Id: 5f316638-012a-4b39-8bf1-76d572d30296
X-Runtime: 0.093281

{
    "age": 21, 
    "created_at": "2018-03-27T02:03:52.240Z", 
    "gender": "female", 
    "id": 2, 
    "lat": 45.8, 
    "long": 87.9, 
    "name": "ciclano", 
    "report": 1, 
    "updated_at": "2018-03-27T02:27:33.971Z"
}

```

### Trade system
Thinking about in the front-end of application, this api was designed with the following steps: the survivor has to choose his resources to exchange, after that, should select the resources of another survivor. So you can start a trade

#### Requests
```sh
http POST :3000/survivors/1/resources_trade water=1 food=2 medication=1

http POST :3000/survivors/4/resources_trade water=2 food=1 ammunition=1

http POST :3000/trades survivor1_id=1 survivor2_id=4
```

Parameters are the same as the resources.


#### Responses
```sh
{
    "ammunition": 0, 
    "created_at": "2018-03-27T02:03:53.131Z", 
    "food": 0, 
    "id": 1, 
    "medication": 0, 
    "survivor_id": 1, 
    "updated_at": "2018-03-27T02:36:07.800Z", 
    "water": 2
}
```

```sh
{
    "ammunition": 1, 
    "created_at": "2018-03-27T02:03:53.398Z", 
    "food": 0, 
    "id": 4, 
    "medication": 0, 
    "survivor_id": 4, 
    "updated_at": "2018-03-27T02:37:21.017Z", 
    "water": 2
}

```

```sh
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"08fcc79b7d53ed84bd6b0d24a1e4a81c"
Transfer-Encoding: chunked
X-Request-Id: 6016ad0c-6178-4968-8886-53f9bf3410ee
X-Runtime: 0.368972

{
    "created_at": "2018-03-27T02:42:36.710Z", 
    "id": 2, 
    "survivor1_id": 1, 
    "survivor2_id": 4, 
    "updated_at": "2018-03-27T02:42:36.710Z"
}

```

### Report System
### GET /reports
To have the data of all the information and statistics:

```sh
http :3000/reports
```


```sh
HTTP/1.1 200 OK
Cache-Control: max-age=0, private, must-revalidate
Content-Type: application/json; charset=utf-8
ETag: W/"9e0168a5bb6260ffecd1c390bba70570"
Transfer-Encoding: chunked
X-Request-Id: 185d0edf-8771-4155-83b5-45da034fddc3
X-Runtime: 0.015341

{
    "meanresources": {
        "ammunition": 6.0, 
        "food": 6.0, 
        "medication": 3.4, 
        "water": 3.2
    }, 
    "perc_infec": 20.0, 
    "perc_not_infect": 80.0, 
    "point_loss": 6
}

```