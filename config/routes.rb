Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
	#root to: "api/v1/survivors"


	namespace :api do
		namespace :v1 do
			resources :survivors, except:  [:destroy] do
				resources :resources, only:  [:index]
				resources :resources_trade , only: [:index,:create]

				match 'flaginfects', to: 'survivors#reportInf', via: [:get]
				match 'updatelocation', to: 'survivors#lastposition', via: [:post]

			end

			resources :trades, only: [:index, :create]
			match 'reports', to: 'sysreports#index', via: [:get]

		end
	end


end
